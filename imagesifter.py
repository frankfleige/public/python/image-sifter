import argparse
from datetime import datetime
from os import path, mkdir
from pathlib import Path
from shutil import move

import exifread
import pytz

_destination_path_cache = {}


def exif_datetime_from_file(p: str, timezone: str = 'UTC'):
    """
    Will try to read EXIF data from the given file and return the creation date,
    defined by 'EXIF DateTimeOriginal', as a datetime object.

    When creating the datetime object, the given timezone will be assumed for the datetime string given
    by 'EXIF DateTimeOriginal'.

    If no exif data could be read from the file, or the EXIF data 'EXIF DateTimeOriginal' is not present,
    the function will return no result.

    :param str p: The path to the file from which the exif information should be read
    :param str timezone: The timezone that should be used for the datetime object to create. Default is 'UTC'.
    :return: A datetime object with date and time given by 'EXIF DateTimeOriginal', and the given timezone.
    If no EXIF information could be read, the function will return ``None``
    """
    try:
        file = open(p, 'rb')
        tags = exifread.process_file(file)
        file.close()
    except OSError:
        return None
    if 'EXIF DateTimeOriginal' not in tags:
        return None
    exif_datetime = tags.get('EXIF DateTimeOriginal')
    datetime_obj = datetime.strptime(exif_datetime.values, '%Y:%m:%d %H:%M:%S')
    return datetime_obj.astimezone(pytz.timezone(timezone))


def created_datetime_from_file(p: str, timezone='UTC'):
    """
    Will return the create datetime of the given file as datetime object.
    :param str p: The path to the file whose creation date should be read.
    :param str timezone: The timezone the creation datetime should be converted to.
    :return: A datetime object, representing the creation datetime of the given file.
    """
    try:
        d2 = path.getctime(p)
        return datetime.fromtimestamp(d2, pytz.timezone(timezone))
    except OSError:
        return None


def datetime_from_file(p: str, timezone='UTC'):
    """
    Will try to read the creation date either from EXIF information or the files metadata itself

    :param str p: path to file
    :param str timezone: The timezone to use when creating the datetime object
    :return: will either return the datetime, read from the EXIF data, or the creation date
    """
    return exif_datetime_from_file(p, timezone) or created_datetime_from_file(p, timezone)


def get_file_list(p: str):
    """
    Generator that returns each absolute path of a file, contained within the given directory path.
    If the path points to a file, only this absolute file path will be returned

    :param p: path to a directory or file
    :return: absolute file path
    """
    p = Path(p)
    if p.is_file():
        yield p.absolute()
    else:
        for x in p.iterdir():
            if x.is_file():
                yield str(x.absolute())


def destination_for_file(source: str, destination: str, timezone='UTC'):
    """
    Will build the path, the source should be moved to.
    If the path does not yet exists, it will be created.
    :param source: Source path
    :param destination: Basic destination path
    :param timezone: Timezone that should be used when parsing dates
    :return:
    """
    global _destination_path_cache
    date = datetime_from_file(source, timezone)
    if date is None:
        return None
    cache_key = destination, "-", date.strftime("%Y%m%d")
    if cache_key in _destination_path_cache:
        return _destination_path_cache[cache_key]
    p = Path(destination)
    for part in ["%Y", "%m", "%d"]:
        p = p.joinpath(date.strftime(part))
        if not p.exists():
            mkdir(str(p.absolute()))
    _destination_path_cache[cache_key] = str(p.absolute())
    return _destination_path_cache[cache_key]


def move_file(source: str, destination: str):
    """
    Will move the given file to the given destination.
    :param str source: file to move
    :param str destination: destination to move the file to
    :return:
    """
    move(source, destination)


parser = argparse.ArgumentParser(description='Sift images')
parser.add_argument('destination', metavar='DESTINATION', type=str, help='basic path to move the files to')
parser.add_argument('source', metavar='SOURCE', type=str, help='source to move the files from')
parser.add_argument('--timezone', dest='timezone', type=str, default='Europe/Berlin')
args = parser.parse_args()

success = 0
error = 0
for f in get_file_list(args.source):
    try:
        d = destination_for_file(f, args.destination, args.timezone)
    except OSError as err:
        print(f"ERROR: could not determine destination for file '{f}': : {err}")
        error += 1
        continue
    try:
        move_file(f, d)
    except OSError as err:
        print(f"ERROR: could move file '{f}' to '{d}': {err}")
        error += 1
        continue
    success += 1
    print(f"moved file '{f}' to '{d}'")
total = success + error
print("-----------------------------------------------------------")
print(f"Total files: {total}, succeeded: {success}, errors: {error}")
