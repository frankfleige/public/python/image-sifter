# Image Sifter

Version: 0.1.0

## What it does
* Moves (image) files to the given destination
* At the destination, the files are organized in sub directories, 
based on their creation date
* The creation date is determined from the EXIF information stored within the
image file. 
* If the EXIF information is not available, the creation date will be determined from
the files metadata.

### Example
Assuming the picture `foo.jpeg` was shot on 2020-02-29 13:01:14 UTC time.
The command `imagesifter /bar /foo/pictures/foo.jpeg` will move the file
to `/bar/2020/02/29/foo.jpeg`

## usage
`imagesifter DESTINATION SOURCE`
* The `DESTINATION` must be an existing and writable directory
* The `SOURCE` can either be a directory or a file

## Build executable
`pyinstaller imagesifter.py`

## What's next
* add support for copying files instead of moving them
* support N sources

## ChangeLog
### 2020-02-29 - Version 0.1.0
* initial release 